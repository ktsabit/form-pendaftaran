<html>
<head>
	<title>FORM PENDAFTARAN</title>
</head>
<body>
	<h2>PENDAFTARAN SEMINAR</h2>
	<br/>
	<a href="index.php">KEMBALI</a>
	<br/>
	<br/>
	<h3>REGISTRASI PESERTA</h3>
	<form method="post" action="tambah_aksi.php">
		<table>
			<tr>			
				<td>Nama</td>
				<td><input type="text" name="name"></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><input type="email" name="email"></td>
			</tr>
			<tr>
				<td>No. HP</td>
				<td><input type="tel" name="phone"></td>
			</tr>
			<tr>
				<td>Tempat Lahir</td>
				<td><input type="text" name="birth"></td>
			</tr>
			<tr>
				<td>Tanggal Lahir</td>
				<td><input type="date" name="bdate"></td>
			</tr>
			<tr>
				<td>Jenis Kelamin</td>
				<td>
					<select name="sex">
						<option value="L">Laki laki</option>
						<option value="P">Perempuan</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Instansi</td>
				<td><input type="text" name="instance"></td>
			</tr>
			<tr>
				<td>Jenis Instansi</td>
				<td>
					<select name="ins_type">
						<option value="BK">Badan Khusus</option>
						<option value="N">Negara</option>
						<option value="S">Swasta</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td><input type="text" name="address"></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="SIMPAN" name="submit"></td>
			</tr>		
		</table>
	</form>
</body>
</html>